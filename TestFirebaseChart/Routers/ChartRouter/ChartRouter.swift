//
//  ChartRouter.swift
//  TestFirebaseChart
//
//  Created by Андрей Катюшин on 20/01/2019.
//  Copyright © 2019 Андрей Катюшин. All rights reserved.
//

import UIKit
import RxSwift

class ChartRouter: ChartRouterProtocol {
    
    private let dataBaseService: DataBaseServiceProtocol
    
    private let disposeBag = DisposeBag()
    
    let startViewController: UIViewController
    
    init() {
        dataBaseService = AppSettings.assembler.resolver.resolve(DataBaseServiceProtocol.self)!
        
        let rootViewController = AppSettings.assembler.resolver.resolve(ChartViewControllerProtocol.self)!
        startViewController = UINavigationController(rootViewController: rootViewController as! UIViewController)
        
        rootViewController.viewModel.needOpenNewRecordTask.subscribe(onNext: { [weak self] (_) in
            self?.openNewRecordTaskViewController()
        }).disposed(by: disposeBag)
    }
    
    private func openNewRecordTaskViewController() {
        var actions = [AlertActionImpl]()
        
        var textFieldTask: UITextField!
        
        actions.append(AlertActionImpl.action(title: "Создать", style: .Default, handler: { [weak self] (_, _) in
            guard let text = textFieldTask.text else {
                return
            }
            self?.dataBaseService.addTask(text: text)
        }))
        
        actions.append(AlertActionImpl.action(title: "Отмена", style: .Cancel, handler: { (_, _) in
        }))
        
        let controller = AlertControllerImpl.alertController(title: "Новая задача",
                                                             message: nil,
                                                             style: .Alert,
                                                             textFieldConfigurationHandler: { (textField) in
                                                                textFieldTask = textField
        },
                                                             actions: actions)
        startViewController.present(controller, animated: true, completion: nil)
    }
    
}
