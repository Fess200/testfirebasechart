//
//  RoutersAssembler.swift
//  TestFirebaseChart
//
//  Created by Андрей Катюшин on 20/01/2019.
//  Copyright © 2019 Андрей Катюшин. All rights reserved.
//

import Foundation
import Swinject

class RoutersAssembler: Assembly {
    
    required init() {}
    
    func assemble(container: Container) {
        container.register(ChartRouterProtocol.self) { _ in
            ChartRouter()
        }
    }
    
}
