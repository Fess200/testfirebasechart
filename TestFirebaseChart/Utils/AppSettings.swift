//
//  AppSettings.swift
//  TestFirebaseChart
//
//  Created by Андрей Катюшин on 20/01/2019.
//  Copyright © 2019 Андрей Катюшин. All rights reserved.
//

import Foundation
import Swinject

class AppSettings {
    
    static let assembler = Assembler([ChartAssembler(),
                                      RoutersAssembler()])
    
    static func setup() {
        GoogleSettings.setup()
    }
    
}
