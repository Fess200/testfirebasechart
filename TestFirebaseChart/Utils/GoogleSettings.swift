//
//  GoogleSettings.swift
//  TestFirebaseChart
//
//  Created by Андрей Катюшин on 20/01/2019.
//  Copyright © 2019 Андрей Катюшин. All rights reserved.
//

import Foundation
import Firebase

class GoogleSettings {
    
    static func setup() {
        FirebaseApp.configure()
    }
    
}
