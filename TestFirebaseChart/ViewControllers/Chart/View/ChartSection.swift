//
//  ChartSection.swift
//  TestFirebaseChart
//
//  Created by Андрей Катюшин on 20/01/2019.
//  Copyright © 2019 Андрей Катюшин. All rights reserved.
//

import Foundation

class ChartSection {
    let name: String
    let tasks: [Task]
    
    init(name: String, tasks: [Task]) {
        self.name = name
        self.tasks = tasks
    }
    
}
