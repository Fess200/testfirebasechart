//
//  ChartViewControllerProtocol.swift
//  TestFirebaseChart
//
//  Created by Андрей Катюшин on 20/01/2019.
//  Copyright © 2019 Андрей Катюшин. All rights reserved.
//

protocol ChartViewControllerProtocol {
    
    var viewModel: ChartViewModelProtocol { get }
    
}
