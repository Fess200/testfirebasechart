//
//  ChartViewController.swift
//  TestFirebaseChart
//
//  Created by Андрей Катюшин on 20/01/2019.
//  Copyright © 2019 Андрей Катюшин. All rights reserved.
//

import UIKit
import RxSwift
import SnapKit

class ChartViewController: UIViewController, ChartViewControllerProtocol {
    
    private let disposeBag = DisposeBag()
    
    private var chartView: UITableView!
    
    private var dataDisplayChart: DataDisplayChart!
    
    let viewModel: ChartViewModelProtocol
    
    init(viewModel: ChartViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupInterface()

        dataDisplayChart = DataDisplayChart(viewModel: viewModel, chart: chartView)
        
        setupObservers()
    }
    
    private func setupInterface() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Добавить",
                                                            style: .done,
                                                            target: self,
                                                            action: #selector(addTask))
        
        chartView = UITableView()
        chartView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(chartView)
        chartView.snp.makeConstraints { (make) in
            make.edges.equalTo(view)
        }
    }
    
    private func setupObservers() {
        viewModel.needReloadChart.subscribe(onNext: { [weak self] (tasks) in
            self?.dataDisplayChart.reload(data: tasks)
        }).disposed(by: disposeBag)
    }
    
    @objc private func addTask() {
        viewModel.addTask()
    }

}
