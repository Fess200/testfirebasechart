//
//  DataDisplayChart.swift
//  TestFirebaseChart
//
//  Created by Андрей Катюшин on 20/01/2019.
//  Copyright © 2019 Андрей Катюшин. All rights reserved.
//

import Foundation
import UIKit

class DataDisplayChart: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    private var data = [ChartSection]()
    
    private let cellReuseIdentifier = "ChartCell"
    
    let chart: UITableView
    
    let viewModel: ChartViewModelProtocol
    
    init(viewModel: ChartViewModelProtocol, chart: UITableView) {
        self.chart = chart
        self.viewModel = viewModel
        
        super.init()
        
        self.chart.delegate = self
        self.chart.dataSource = self
        self.chart.register(ChartCell.self, forCellReuseIdentifier: cellReuseIdentifier)
    }
    
    func reload(data: [Task]) {
        var tasksNoCheck = [Task]()
        var tasksCheck = [Task]()
        data.forEach { (task) in
            if task.check {
                tasksCheck.append(task)
            } else {
                tasksNoCheck.append(task)
            }
        }
        self.data = [ChartSection(name: "Невыполненные", tasks: tasksNoCheck),
                     ChartSection(name: "Выполненные", tasks: tasksCheck)]
        self.chart.reloadData()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return indexPath.section == 0
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let task = data[indexPath.section].tasks[indexPath.row]
        viewModel.remove(task: task)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        if indexPath.section == 0 {
            return .delete
        } else {
            return .none
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return data[section].name
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data[section].tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let task = data[indexPath.section].tasks[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath)
        if let cell = cell as? ChartCell {
            cell.task = task
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let task = data[indexPath.section].tasks[indexPath.row]
            viewModel.check(task: task)
        }
    }
    
}
