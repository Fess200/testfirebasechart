//
//  ChartAssembler.swift
//  TestFirebaseChart
//
//  Created by Андрей Катюшин on 20/01/2019.
//  Copyright © 2019 Андрей Катюшин. All rights reserved.
//

import Foundation
import Swinject

class ChartAssembler: Assembly {
    
    required init() {}
    
    func assemble(container: Container) {
        container.register(DataBaseServiceProtocol.self) { _ in
            DataBaseService()
        }
        container.register(ChartViewModelProtocol.self) { _ in
            ChartViewModel(dataBase: container.resolve(DataBaseServiceProtocol.self)!)
        }
        container.register(ChartViewControllerProtocol.self) { _ in
            ChartViewController(viewModel: container.resolve(ChartViewModelProtocol.self)!)
        }
    }
    
}
