//
//  ChartViewModelProtocol.swift
//  TestFirebaseChart
//
//  Created by Андрей Катюшин on 20/01/2019.
//  Copyright © 2019 Андрей Катюшин. All rights reserved.
//

import RxSwift

protocol ChartViewModelProtocol {
    
    var needReloadChart: PublishSubject<[Task]> {get}
    
    var needOpenNewRecordTask: PublishSubject<Void> {get}
    
    func addTask()
    
    func remove(task: Task)
    
    func check(task: Task)
    
}
