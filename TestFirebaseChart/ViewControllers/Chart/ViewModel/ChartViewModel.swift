//
//  ChartViewModel.swift
//  TestFirebaseChart
//
//  Created by Андрей Катюшин on 20/01/2019.
//  Copyright © 2019 Андрей Катюшин. All rights reserved.
//

import Foundation
import RxSwift

class ChartViewModel: ChartViewModelProtocol {
    
    private let dataBase: DataBaseServiceProtocol
    
    private let disposeBag = DisposeBag()
    
    let needReloadChart = PublishSubject<[Task]>()
    
    let needOpenNewRecordTask = PublishSubject<Void>()
    
    init(dataBase: DataBaseServiceProtocol) {
        self.dataBase = dataBase
        
        setupObservers()
    }
    
    func addTask() {
        needOpenNewRecordTask.onNext(())
    }
    
    func remove(task: Task) {
        dataBase.remove(task: task)
    }
    
    func check(task: Task) {
        dataBase.check(task: task)
    }
    
    private func setupObservers() {
        dataBase.updateTasksSignal.subscribe(onNext: { [weak self] (tasks) in
            self?.needReloadChart.onNext(tasks)
        }).disposed(by: disposeBag)
    }
    
}
