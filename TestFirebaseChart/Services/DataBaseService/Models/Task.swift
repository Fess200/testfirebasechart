//
//  Task.swift
//  TestFirebaseChart
//
//  Created by Андрей Катюшин on 20/01/2019.
//  Copyright © 2019 Андрей Катюшин. All rights reserved.
//

import Foundation
import ObjectMapper

enum TaskAttributes {
    case text
    case check
    
    func toJson() -> String {
        switch self {
        case .text:
            return "text"
        case .check:
            return "check"
        }
    }
}

class Task: NSObject, Mappable {
    
    var id: String!
    var text: String?
    var check: Bool = false
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        text <- map[TaskAttributes.text.toJson()]
        check <- map[TaskAttributes.check.toJson()]
    }
    
}
