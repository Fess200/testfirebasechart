//
//  DataBaseServiceProtocol.swift
//  TestFirebaseChart
//
//  Created by Андрей Катюшин on 20/01/2019.
//  Copyright © 2019 Андрей Катюшин. All rights reserved.
//

import RxSwift

protocol DataBaseServiceProtocol {
    
    var updateTasksSignal: PublishSubject<[Task]> { get }
    
    func addTask(text: String)
    
    func remove(task: Task)
    
    func check(task: Task)
    
}
