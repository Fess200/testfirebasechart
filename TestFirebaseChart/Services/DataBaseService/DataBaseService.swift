//
//  DataBaseService.swift
//  TestFirebaseChart
//
//  Created by Андрей Катюшин on 20/01/2019.
//  Copyright © 2019 Андрей Катюшин. All rights reserved.
//

import Foundation
import RxSwift
import FirebaseDatabase

class DataBaseService: DataBaseServiceProtocol {
    
    private let ref: DatabaseReference = Database.database().reference()
    
    private let tableTasksName = "tasks"
    
    let updateTasksSignal = PublishSubject<[Task]>()
    
    init() {
        setupObservers()
    }
    
    func addTask(text: String) {
        let tasksReference = ref.child(tableTasksName)
        if let key = tasksReference.childByAutoId().key {
            let newTaskReference = tasksReference.child(key)
            newTaskReference.setValue([TaskAttributes.text.toJson(): text,
                                       TaskAttributes.check.toJson(): false])
        }
    }
    
    func remove(task: Task) {
        let tasksReference = ref.child(tableTasksName)
        tasksReference.child(task.id).removeValue()
    }
    
    func check(task: Task) {
        let tasksReference = ref.child(tableTasksName)
        tasksReference.child(task.id).child(TaskAttributes.check.toJson()).setValue(true)
    }
    
    private func setupObservers() {
        ref.child(tableTasksName).observe(.value) { [weak self] (snapshot) in
            var tasks = [Task]()
            snapshot.children.forEach({ (children) in
                if let children = children as? DataSnapshot {
                    if let json = children.value as? [String: Any] {
                        let task = Task(JSON: json)!
                        task.id = children.key
                        tasks.append(task)
                    }
                }
            })
            self?.updateTasksSignal.onNext(tasks)
        }
    }
    
}
