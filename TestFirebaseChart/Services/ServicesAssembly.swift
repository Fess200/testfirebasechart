//
//  ServicesAssembly.swift
//  TestFirebaseChart
//
//  Created by Андрей Катюшин on 20/01/2019.
//  Copyright © 2019 Андрей Катюшин. All rights reserved.
//

import Foundation
import Swinject

class ServicesAssembly: Assembly {
    
    required init() {}
    
    func assemble(container: Container) {
        container.register(DataBaseServiceProtocol.self) { _ in
            DataBaseService()
        }.inObjectScope(.container)
    }
    
}
