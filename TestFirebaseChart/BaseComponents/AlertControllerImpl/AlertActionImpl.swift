import UIKit

enum AlertActionStyleImpl {
    case Default
    case Cancel
    case Destructive
}

typealias HandlerActionBlock = ((_ action: AlertActionImpl, _ previewViewController: UIViewController)->())

class AlertActionImpl {
    
    var title: String!
    var style: AlertActionStyleImpl!
    var handler: HandlerActionBlock?
    
    static func action(title: String, style: AlertActionStyleImpl, handler: HandlerActionBlock?) -> AlertActionImpl {
        let action = AlertActionImpl()
        action.title = title
        action.style = style
        action.handler = handler
        return action
    }
    
}
