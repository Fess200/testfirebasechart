import UIKit

enum AlertControllerStyleImpl {
    case Alert
    case ActionSheet
}

class AlertControllerImpl {
    
    static func alertController(title: String?, message: String?, style: AlertControllerStyleImpl, sourceView: UIView? = nil, textFieldConfigurationHandler: ((UITextField) -> ())? = nil, actions: [AlertActionImpl]) -> UIViewController {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: convertStyle(controllerStyle: style))
        
        if style == .Alert && textFieldConfigurationHandler != nil {
            alertController.addTextField(configurationHandler: textFieldConfigurationHandler)
        }
        
        actions.forEach { (actionImpl) in
            let action = UIAlertAction(title: actionImpl.title, style: convertStyle(actionStyle: actionImpl.style), handler: { (_) in
                actionImpl.handler?(actionImpl, alertController)
            })
            alertController.addAction(action)
        }
        
        if style == .ActionSheet && UIDevice.current.userInterfaceIdiom == .pad {
            guard let sourceView = sourceView else {
                assert(false, "sourceView is nill")
                return alertController
            }
            setupActionSheetForIPad(alertController: alertController, sourceView: sourceView)
        }
        
        return alertController
    }
    
    // MARK : Private
    
    static func convertStyle(actionStyle: AlertActionStyleImpl) -> UIAlertAction.Style {
        switch (actionStyle) {
        case .Default:
            return .default
        case .Cancel:
            return .cancel
        case .Destructive:
            return .destructive
        }
    }
    
    static func convertStyle(controllerStyle: AlertControllerStyleImpl) -> UIAlertController.Style {
        switch (controllerStyle) {
        case .ActionSheet:
            return .actionSheet
        case .Alert:
            return .alert
        }
    }
        
    static func setupActionSheetForIPad(alertController: UIAlertController, sourceView: UIView) {
        alertController.popoverPresentationController?.sourceView = sourceView
        alertController.popoverPresentationController?.sourceRect = CGRect(origin: sourceView.center, size: CGSize.zero)
        alertController.popoverPresentationController?.permittedArrowDirections = .any
    }
        
}

